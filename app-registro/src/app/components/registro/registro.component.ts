import { ApiService } from './../../services/api.service';
import { Registro } from './../../models/Registro';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  listRegistro:Registro[]=[];
  constructor(private FrmRegistro: FormBuilder, private api:ApiService ) { 
     this.todosLosRegistros();
  }
  consulta=this.FrmRegistro.group({
     key:''
  });

  todosLosRegistros():void{
    this.api.traerLibros().subscribe((data:Registro[])=>{
      console.log("data",data);
      this.listRegistro=data;
    })
}
actualizarRegistro():void{
    this.api.actualizar().subscribe((data:Registro[])=>{
      this.listRegistro=data;
    })
}
  ngOnInit(): void {
  }
  

}
