import { Component, OnInit } from '@angular/core';
import { ApiService } from './../../services/api.service';
import { Registro } from './../../models/Registro';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-crear-registro',
  templateUrl: './crear-registro.component.html',
  styleUrls: ['./crear-registro.component.css']
})
export class CrearRegistroComponent implements OnInit {
  datosRegistro={
    id:0,
    nombre:'',
    apellido:'',
    procesar:false,
  };
  
  listRegistro:Registro[]=[];
  constructor(private FrmRegistro: FormBuilder, private api:ApiService ) { 
     this.todosLosRegistros();
  }
  consulta=this.FrmRegistro.group({
    nombre:'',
    apellido:'',
    procesar:'',
  });
  todosLosRegistros():void{

    this.api.traerLibros().subscribe((data:Registro[])=>{
      console.log("data",data);
      this.listRegistro=data;
    })
}
crearRegistro():void{
  console.log("Entra a crear");
  const datos = {
       nombre:this.datosRegistro.nombre,
       apellido:this.datosRegistro.apellido,
       procesar:this.datosRegistro.procesar,
    };
   this.api.crearRegistro(datos).subscribe((data:Registro)=>{
        console.log(data);
        if(data!=null){
          this.todosLosRegistros();
          this.limpiarTextos();
        }
   });
  }
  limpiarTextos():void{
   this.datosRegistro={
      id:0,
      nombre:'',
      apellido:'',
      procesar:false,
    };
  }

  ngOnInit(): void {
  }

}
