import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegistroComponent } from './components/registro/registro.component';
import { MenuComponent } from './components/menu/menu.component';
import { ReactiveFormsModule } from '@angular/forms' ;
import {​​​​​ HttpClientModule ,HttpHandler}​​​​​ from '@angular/common/http';
import { CrearRegistroComponent } from './components/crear-registro/crear-registro.component';

@NgModule({
  declarations: [
    AppComponent,
    RegistroComponent,
    MenuComponent,
    CrearRegistroComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
