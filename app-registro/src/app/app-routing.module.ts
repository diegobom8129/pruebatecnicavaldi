import { RegistroComponent } from './components/registro/registro.component';
import { CrearRegistroComponent } from './components/crear-registro/crear-registro.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {​​​​​ path: 'Registros', component: RegistroComponent }​​​​​, 
  { path: 'Crear-Registro', component: CrearRegistroComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
