import {Registro} from './../models/Registro';
import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  Url="http://localhost:8080/Registros";
  constructor(private http: HttpClient) { 

  }
  traerLibros(){
       return this.http.get<Registro[]>(this.Url+"/registros");
  }
  crearRegistro(data){
    console.log("datoser",data);
    return this.http.post(this.Url+"/guardaregistro",data);
  }
  actualizar(){
    return this.http.get<Registro[]>(this.Url+"/actualizaRegistros");
  }
}
