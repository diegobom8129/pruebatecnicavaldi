package com.example.demo.model.dao;

import java.util.List;
import java.util.Optional;

import com.example.demo.model.entitys.Registro;

public interface IResgistroService {
	
	public List<Registro> getRegistro();
	public Registro guardarRegistro(Registro registro);
	public Registro buscarById(Long Id);
	public Registro actualizarRegistro(Registro registro);
	public List<Registro> findByNombre(String nombre);

}
