package com.example.demo.model.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.model.dao.IResgistroService;
import com.example.demo.model.entitys.Registro;
@RestController

@CrossOrigin(origins="*",methods= {RequestMethod.POST,RequestMethod.GET,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping(value="Registros")

public class ResgistroController {

	@Autowired
	private IResgistroService registroService;
	
	@GetMapping(value="/registros",produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Registro> getRegistro() {
		return registroService.getRegistro();
	}
	@PostMapping(value="/guardaregistro")
	public Registro GuardarRegistro(@RequestBody Registro registro) {
		return registroService.guardarRegistro(registro);
	}
	@PutMapping(value="/actualizaRegistro/{id}")
	public Registro ActualizaRegistro(@PathVariable(name="id") Long id,@RequestBody Registro registro) {
		Registro registroAnterio= new Registro();
		Registro registroNuevo= new Registro();
		registroAnterio=registroService.buscarById(id);
		registroAnterio.setApellido(registro.getApellido());
		registroAnterio.setNombre(registro.getNombre());
		registroAnterio.setprocesar(registro.isprocesar());
		registroNuevo=registroService.actualizarRegistro(registroAnterio);
		return registroNuevo;
	}
	@GetMapping(value="/actualizaRegistros")
	public List<Registro> ActualizaRegistro() {
		List<Registro> listActualizar= new ArrayList<Registro>();
		for(Registro r : registroService.getRegistro()) {
			Registro registroNuevo= new Registro();
			r.setprocesar(true);
			registroNuevo=registroService.actualizarRegistro(r);
			listActualizar.add(registroNuevo);
		}
		return listActualizar;
	}
	@GetMapping(value="/buscar/{nombre}")
	public List<Registro> getnombre(@PathVariable(name="nombre") String n) {
		return registroService.findByNombre(n);
	}
}
