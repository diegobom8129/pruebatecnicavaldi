package com.example.demo.model.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.dao.IResgistroService;
import com.example.demo.model.dao.IRegistroDAO;
import com.example.demo.model.entitys.Registro;

@Service
public class RegistroService implements IResgistroService {

	@Autowired
	private IRegistroDAO registroDao;
	
	public List<Registro> getRegistro(){
		return (List<Registro>) registroDao.findAll();
	}

	public Registro guardarRegistro(Registro registro) {	
		return registroDao.save(registro);
	}
	
	public Registro buscarById(Long Id) {
		return registroDao.findById(Id).get();
	}
	public Registro actualizarRegistro(Registro registro) {	
		return registroDao.save(registro);
	}
	public List<Registro> findByNombre(String nombre){
		return registroDao.findByNombre(nombre);
	}
}
