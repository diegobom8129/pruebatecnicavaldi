package com.example.demo.model.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

import com.example.demo.model.entitys.Registro;
@Repository
public interface IRegistroDAO extends CrudRepository<Registro, Long>{
  public List<Registro> findByNombre(String nombre);
}
